module.exports={
  init:function(options){
    module.exports.fs = require('fs');
    module.exports.Terser = require("terser");
    module.exports.addslashes=function(string) {
        return string.replace(/\\/g, '\\\\').
            replace(/\u0008/g, '\\b').
            replace(/\t/g, '\\t').
            replace(/\n/g, '\\n').
            replace(/\f/g, '\\f').
            replace(/\r/g, '\\r').
            replace(/'/g, '\\\'').
            replace(/"/g, '\\"');
    }
  },
  setOptions:function(options){
    module.exports.options=options;
  },
  loadFiles:function(files,vars){
    var filesdata='';
    files.forEach(function(file) {
      try{
        filesdata+=module.exports.fs.readFileSync(file,'utf8')
      }catch(e){
        console.log(e.message);
      }
    });
    if(vars&&filesdata){
      for(var key in vars){
        filesdata=filesdata.replace(new RegExp('~'+key+'~', 'g'),vars[key]);
      }
    }
    return filesdata;
  },
  getConfVars:function(conf){
    if(!conf.vars) conf.vars={};
    conf.vars.app_id=conf.id;
    conf.vars.code=module.exports.options.code;
    if(module.exports.options&&module.exports.options.vars) Object.assign(conf.vars,module.exports.options.vars);
    return conf.vars;
  },
  parseConf:function(conf){//build conf of js:[],css:[],templates:[]
    var fileconf={
      js:[],
      css:[],
      templates:[]
    }
    function getComponentFiles(path){
      var appjs=path+'.js';
      if (module.exports.fs.existsSync(appjs)){
        fileconf.js.push(appjs);
      }
      var appcss=path+'.css';
      if (module.exports.fs.existsSync(appcss)){
        fileconf.css.push(appcss);
      }
      var apptemplate=path+'.ejs';
      if (module.exports.fs.existsSync(apptemplate)){
        fileconf.templates.push(apptemplate);
      }
      var apptemplate=path+'.templates';
      if (module.exports.fs.existsSync(apptemplate)){
        fileconf.templates.push(apptemplate);
      }
    }
    //add in the app info first!
    getComponentFiles('static/'+conf.id);
    if(conf.views&&conf.views.length){
      for (var i = 0; i < conf.views.length; i++) {
        var view=conf.views[i];
        getComponentFiles('static/views/'+view+'/'+view);
      }
    }
    return fileconf;
  },
  getComponentFiles(fileconf,appjs,options,conf){
    if(!fileconf) fileconf={};
      if(!options) options={};
      if (module.exports.fs.existsSync(appjs)){
        var content=module.exports.fs.readFileSync(appjs,'utf8');
        var map={
          route:'js',
          script:'js',
          templates:'templates',
          dependencies:'js',
          style:'css'
        }
        var remap={}
        for (var key in map){
          var code='';
          var mapto=map[key];
          if(['dependencies','route'].indexOf(key)==-1) remap[mapto]=key;
          if(!fileconf[mapto]) fileconf[mapto]='';
          var start=content.search('<'+key+'>');
          if(start>=0){
            var end=content.search('</'+key+'>');
            if(end){
              code=content.substring(start+('<'+key+'>').length,end);
              if(options[key]){
                if(options[key].transform){
                  switch(options[key].transform){
                    case 'dependencies':
                      var parts=code.split('\n');
                      if(parts&&parts.length){
                        parts=parts.filter(n => n);
                        code=JSON.stringify(parts);
                      }
                    break;
                    default:
                      console.log('invalid transform ['+options[key].transform+']')
                    break;
                  }
                }
                if(options[key].wrap){
                  if(options[key].wrap.start) code=options[key].wrap.start+code;
                  if(options[key].wrap.end) code=code+options[key].wrap.end;
                }
              }
              if(mapto=='templates'){
                code=code.replace(/(\r\n|\n|\r|\t)/gm,"");
                // minified=Terser.minify(code,{});
                // code=minified.code+';';
              }
              if(mapto=='js'){
                if(module.exports.options.prod&&key!='dependencies'){
                  minified=module.exports.Terser.minify(code,{});
                  code=minified.code+';';
                }else{
                  code=code+';';//dev
                }
              }
              // if(key=='dependencies') console.log('dependencies:'+code)
              // if(key=='script'&&appjs=='app/views/homepage.view') console.log(code)
              fileconf[mapto]+=code;
            }
          }
        }
        //console.log(fileconf.js);
      }else{
        console.log('File ['+appjs+'] does not exist');
      }
      if(options.recombine){
        var out='';
        for(var key in remap){
          if(fileconf[key]){
            out+='<'+remap[key]+'>'+fileconf[key]+'</'+remap[key]+'>';
          }
        }
        if(conf){
          var vars=module.exports.getConfVars(conf);
          if(vars){
            for(var key in vars){
              out=out.replace(new RegExp('~'+key+'~', 'g'),vars[key]);
            }
          }
        }
        return out;
      }
      return fileconf;
    },
  getViewConfig:function(conf,view,options){
    if(!options) options={}
    var opts={
        script:{
          wrap:{
            start:"phi.loadView('"+conf.id+"','"+view+"',function(options){",
            end:"})"
          }
        },
        route:{
          wrap:{
            start:"phi.loadRoute('"+conf.id+"','"+view+"',function(options){",
            end:"})"
          }
        },
        dependencies:{
          transform:'dependencies',
          wrap:{
            start:"phi.loadDependencies('"+conf.id+"','"+view+"',",
            end:")"
          }
        }
      }
      return Object.assign({},opts,options);
  },
  parseConf2:function(conf){//build conf of js:'',css:'',templates:''
    var fileconf={
      js:'',
      css:'',
      templates:''
    }
    //add in the app info first!
    module.exports.getComponentFiles(fileconf,'app/'+conf.id+'.app',{
          script:{
            wrap:{
              start:"phi.loadApp('"+conf.id+"',function(options){",
              end:"})"
            }
          }
        });
    if(conf.views&&conf.views.length){
      for (var i = 0; i < conf.views.length; i++) {
        var view=conf.views[i];
        module.exports.getComponentFiles(fileconf,'app/views/'+view+'.view',module.exports.getViewConfig(conf,view));
      }
    }
    var vars=module.exports.getConfVars(conf);
    if(vars){
      for(var key in vars){
        for(var type in fileconf){
            fileconf[type]=fileconf[type].replace(new RegExp('~'+key+'~', 'g'),vars[key]);
        }
      }
    }
    if(fileconf.templates) fileconf.templates=module.exports.addslashes(fileconf.templates)
    return fileconf;
  }
}