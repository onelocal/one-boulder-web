# One Boulder

![hummingbird](https://s3.amazonaws.com/wearenectar/static/hyperspace.jpg "hummingbird")

# Getting Started

```
git clone https://gitlab.com/onelocal/one-boulder-web
cd one-boulder-web
npm install
npm link
serve-flower dev_tom
```

Then go to [localhost:3333](http://localhost:3333) in your browser of choice.

# Playing with some code

Go to the code editor of your choice.

The relevant files for just getting started are in the 
```
/app/views (directory)
```
Open /app/views/profile.view

View your profile in the web browser

I recommend having the code and the app view in the same window at the same time, like this.

![docs](https://s3.amazonaws.com/one-light/static/docs.png "docs")


You should see the change happen immediately in your browser.  

*note* I have not perfected all the error catching yet, so its possible that you may run into a situation where things arent changing or loading.  First fix is reload the page and try again.  If its still broken, there is probably an issue with templates or the logic.  check your developers console to look for messages that may help.

# Mobile Apps for testing

[Coming soon!]