#!/usr/bin/env node
const { exec } = require('child_process');
const axios = require('axios')
const api=require('./api.js');
api.init();
var env=process.argv[2];
if(!env) return console.warn('Environment must be specified');
var conf=JSON.parse(api.loadFiles(['conf.json']));
if(!api.loadFiles(['creds.json'])){
	console.warn('creds.json is not set!');
	return false;
}
var creds=JSON.parse(api.loadFiles(['creds.json']));
if(!conf) return console.warn('invalid conf.json');
if(!conf.envs||!conf.envs[env]) return console.warn('invalid Environment ['+env+']');
var environment=conf.envs[env];
api.setOptions({
  vars:{
    api:environment.url.replace('https://',''),
    prod:environment.prod,
    flower_id:conf.id,
    core:conf.core,
    appid:conf.appid
  }
});
exec('git rev-parse --abbrev-ref HEAD', (err, stdout, stderr) => {
    if (err) {
        // handle your error
    }
    var branch=stdout.trim();
	var appEntry=conf.appEntry;
	appEntry.app=conf.id;
	var fileconf=api.parseConf2(conf);
	var publish={
	    "key":conf.id,
	    "code":fileconf,
	    "entry":conf.entry,
	    "appEntry":appEntry,
	    "branch":branch
	}
	console.log('publishing brach ['+branch+']...');
	axios.post(environment.url+'/flower/publish/'+conf.id, {
			developer:creds.developer,
			project:conf.id,
	    token: creds.token,//coming soon, need to validate person has permissions to "publish" to the branch
			conf:publish
	}).then(function(res){
		console.log('Successfully Published!');
		console.log(JSON.stringify(res.data,null,4));
		process.exit(0);
	}).catch(function(error){
		console.log('Error publishing');
		console.error(error);
		process.exit(0);
	})
});